CREATE TABLE model_predictions (
    user_id BIGINT PRIMARY KEY,
    recipe_id BIGINT,
    rating FLOAT,
    predictions FLOAT
);