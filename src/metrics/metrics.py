import click
from typing import List, Any
import logging
from sklearn.metrics import mean_squared_error, mean_absolute_error
import pandas as pd
from datetime import datetime
import json
from os import path

from src.config import PATH_PROCESSED, PATH_REPORTS


def evaluate(y_test: List[float], y_pred: List[float]) -> tuple[Any, Any]:
    """Evaluate predictions."""
    rmse = mean_squared_error(y_test, y_pred, squared=False)
    mae = mean_absolute_error(y_test, y_pred)
    return rmse, mae


def metrics_evaluate(
    input_filepath_predictions: str,
    output_filepath: str = PATH_REPORTS
     ) -> dict[str, Any]:
    # Load dataset
    df = pd.read_csv(input_filepath_predictions)
    rmse, mae = evaluate(df["rating"].values, df["predictions"].values)
    save_report_results(f"{output_filepath}/metrics_report.json", rmse, mae)
    return {"RMSE": rmse, "MAE": mae}


def save_report_results(filename: str, rmse: float, mae: float) -> None:
    """Save report results to filename."""
    key = datetime.now().strftime("%Y.%m.%d %H:%M:%S")
    report = {key: [rmse, mae]}

    if path.isfile(filename) is False:
        data = {}
    else:
        with open(filename) as fp:
            data = json.load(fp)

    data.update(report)

    with open(filename, 'w') as json_file:
        json.dump(data, json_file, indent=4)


@click.command()
@click.argument("input_filepath_predictions",
                default=f"{PATH_PROCESSED}/predictions_test.csv",
                type=click.Path(exists=True))
@click.argument("output_filepath",
                default=f"{PATH_REPORTS}",
                type=click.Path(exists=True))
def main(
    input_filepath_predictions: str,
    output_filepath: str
     ) -> None:
    """Evaluate model predictions on test data."""
    logger = logging.getLogger(__name__)
    logger.info("evaluating model predictions")
    metrics = metrics_evaluate(input_filepath_predictions, output_filepath)
    logger.info(metrics)


if __name__ == "__main__":

    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    main()
