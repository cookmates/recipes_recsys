# -*- coding: utf-8 -*-
from typing import List, Any, Union
import click
import logging

import pandas as pd
from numpy import ndarray
from surprise import dump
from src.config import PATH_PROCESSED, PATH_MODELS


def predict_all(model: Any,
                df_recipes: pd.DataFrame,
                user_id: str) -> Union[Any, pd.DataFrame]:
    """Predict ratings for all recipes for a given user_id."""
    all_recipes = df_recipes["id"].unique()

    predictions = []
    for recipe_id in all_recipes:
        predictions.append(model.predict(user_id, recipe_id))

    predictions_df = pd.DataFrame(predictions)
    predictions_df.sort_values("est", ascending=False, inplace=True)

    return predictions_df


def predict_model(
    input_filepath_test: str,
    input_model_path: str,
     ) -> pd.DataFrame:

    # load model
    _, model = dump.load(input_model_path)

    # load test data
    test_df = pd.read_csv(input_filepath_test)

    # predict, save predictions to csv

    user_ids = test_df["user_id"].values
    recipes_ids = test_df["recipe_id"].values

    predictions = [model.predict(user_id, recipes_id).est for
                   user_id, recipes_id in zip(user_ids, recipes_ids)]
    test_df["predictions"] = predictions

    return test_df


def get_top_recommendations(df_recipes: pd.DataFrame,
                            predictions_df: pd.DataFrame,
                            tags: List[str],
                            n: int = 5) -> Union[Any, ndarray[Any, Any]]:
    """Gets top n predictions for a given user_id and given tags."""
    mask = True
    for tag in tags:
        mask &= df_recipes[tag]

    recipes_ids = df_recipes[mask]["id"].unique()
    predictions_df = predictions_df[predictions_df["iid"].isin(recipes_ids)]
    top_n_recommendations = predictions_df["iid"].values[:n]
    return top_n_recommendations


@click.command()
@click.argument("input_filepath_test",
                default=f"{PATH_PROCESSED}/interactions_test.csv",
                type=click.Path(exists=True))
@click.argument("input_model_path",
                default=f"{PATH_MODELS}/model.pickle",
                type=click.Path(exists=True))
@click.argument("output_filepath",
                default=f"{PATH_PROCESSED}/predictions_test.csv",
                type=str)
def main(input_filepath_test: str,
         input_model_path: str,
         output_filepath: str) -> None:
    logger = logging.getLogger(__name__)
    logger.info("making predictions for test data")

    test_df = predict_model(input_filepath_test, input_model_path)
    test_df.to_csv(output_filepath, index=False)


if __name__ == "__main__":

    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    main()
