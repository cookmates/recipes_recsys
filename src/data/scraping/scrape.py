import pandas as pd
import time
from tqdm import tqdm
from scraping import PageParse, get_url
# from data.scraping import PageParse, get_url
# import PageParse, get_url
from os.path import exists
from config import START_URL, PATH_TO_URL
from config import PATH_SCRAPED_RECIPE, PATH_SCRAPED_INTERACTIONS
from selenium import webdriver
from typing import Union

Interactions_dict = dict[str, list[Union[int, str, None]]]


def scrape(df_urls: pd.DataFrame,
           path_recipes: str,
           path_interactions: str) -> None:
    '''Collects two dataframe with recipe and reviews data
        using methods of the PageParse class
    '''
    attributes_recipe = [
        "recipe_name",
        "recipe_id",
        "minutes",
        "tags",
        "ingredients",
        "n_ingredients",
        "steps",
        "n_steps",
        "calories",
        "nutrition",
    ]
    interactions: Interactions_dict = {
        "user_id": [],
        "date": [],
        "rating": [],
        "review": [],
        "recipe_id": [],
    }
    temp_recipes = pd.DataFrame(columns=attributes_recipe)
    interactions_df = pd.DataFrame.from_dict(interactions)
    driver = PageParse.Driver(firefox_profile=webdriver.FirefoxProfile())

    for i in tqdm(range(0, len(df_urls["recipe_urls"]))):
        url = df_urls["recipe_urls"][i]
        html = driver.get_html_with_selenium(url)
        scraper = PageParse.PageParse(url, html)

        temp_recipes.loc[i] = [
            getattr(scraper, attr)() for attr in attributes_recipe
        ]
        dict_rev = scraper.interaction_recipe()
        try:
            interactions_df = pd.concat(
                [interactions_df,
                    pd.DataFrame.from_dict(dict_rev)],
                ignore_index=True
            )
        except Exception:
            continue

        if i % 50 == 0:
            print(f"/n{i} links processed")
        time.sleep(3)

    driver.quit()
    temp_recipes["recipe_urls"] = df_urls["recipe_urls"]
    columns = ["recipe_urls"] + attributes_recipe
    temp = temp_recipes[columns]
    AllRecipes_df = temp
    AllRecipes_df.to_csv(path_recipes, index=False)
    interactions_df.to_csv(path_interactions, index=False)
    print(f'All data saved to .csv in: {path_recipes}, {path_interactions}')


if __name__ == "__main__":
    if exists(PATH_TO_URL):
        df_urls = pd.read_csv(PATH_TO_URL)
    else:
        df_urls = get_url.get_url(START_URL)

    scrape(df_urls, PATH_SCRAPED_RECIPE, PATH_SCRAPED_INTERACTIONS)
