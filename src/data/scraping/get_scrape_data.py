import config
from os.path import exists
from pandas import read_csv
import scrape
import get_url


if __name__ == "__main__":

    if exists(config.PATH_TO_URL):
        df_url_to_parse = read_csv(config.PATH_TO_URL)
    else:
        df_url_to_parse = get_url.get_url(config.START_URL, config.PATH_TO_URL)

    scrape.scrape(df_url_to_parse,
                  config.PATH_SCRAPED_RECIPE,
                  config.PATH_SCRAPED_INTERACTIONS)
