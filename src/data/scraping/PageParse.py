from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from numpy import nan
from re import findall
from typing import Union


Interactions_dict = dict[str, list[Union[int, str, None]]]


class Driver(webdriver.Firefox):
    ''' Starts a new local session of Firefox.
        Based on the combination and specificity of the various keyword
        arguments from class WebDriver(RemoteWebDriver), a capabilities
        dictionary will be constructed that is passed to the remote end.

        ATTENTION! Selenium requires a driver to interface with the chosen
        browser. Firefox requires a geckodriver.
        https://github.com/mozilla/geckodriver/releases
        It should be in your PATH.
    '''
    def __init__(self, **kwargs):  # type: ignore
        '''The keyword arguments:
        :param firefox_profile: Deprecated: Instance of "FirefoxProfile"
            object or a string.  If undefined, a fresh profile will be created
            in a temporary location on the system.
        :param firefox_binary: Deprecated: Instance of "FirefoxBinary" or
            full path to the Firefox binary.  If undefined, the system default
            Firefox installation will  be used.
        :param capabilities: Deprecated: Dictionary of desired capabilities.
        :param proxy: Deprecated: The proxy settings to use when communicating
            with Firefox via the extension connection.
        :param executable_path: Deprecated: Full path to override which
            geckodriver binary to use for Firefox 47.0.1 and greater, which
            defaults to picking up the binary from the system path.
        :param options: Instance of "options.Options".
        :param service: (Optional) service instance for managing the starting
            and stopping of the driver.
        :param service_log_path: Deprecated: Where to log information from the
            driver.
        :param service_args: Deprecated: List of args to pass to the driver
            service.
        :param desired_capabilities: Deprecated: alias of capabilities.
            This will make the signature consistent with RemoteWebDriver.
        :param keep_alive: Whether to configure remote_connection.
            RemoteConnection to use HTTP keep-alive.
        '''
        super().__init__(**kwargs)
        self._num_rev_before_scroll = 9
        self.max_review = 30

    def get_html_with_selenium(self, url: str) -> str:
        '''Gets specific page recipe's data from www.allrecipes.com.
            Returns data as a string.

            Clicks the "Show Full Nutrition Label" area.
            Scrolls the page into the viewport of the "load more reviews"
            button, which loads the javascript content and clicks that button
            a specified number of times (in a loop).

            :param url: remote page address
        '''
        try:
            self.get(url)
            time.sleep(6)
        except TimeoutException:
            print("new connection try")
            self.get(url)
            time.sleep(5)
        self.implicitly_wait(5)
        try:
            button_nutrition = self.find_element(
             By.CSS_SELECTOR, ".mntl-nutrition-facts-label__button-text"
            )
            button_nutrition.location_once_scrolled_into_view
            self.execute_script("arguments[0].click();", button_nutrition)
            time.sleep(4)
        except Exception:
            time.sleep(2)

        for _ in range(self.max_review // self._num_rev_before_scroll):
            try:
                self.implicitly_wait(6)
                button_review = self.find_element(
                    By.CSS_SELECTOR, ".feedback-list__load-more-button"
                )
                button_review.location_once_scrolled_into_view
                self.execute_script("arguments[0].click();", button_review)
                time.sleep(5)
            except Exception:
                break
        time.sleep(5)
        html_source = self.page_source
        return html_source


class PageParse:
    '''Parses text from the www.allrecipes.com. and extracts
        the details of the recipe and reviews.
    '''
    def __init__(self, url: str, html: str, max_review: int = 30) -> None:
        self.url = url
        self.max_review = max_review
        self.html_source = html
        self._num_rev_before_scroll: int = 9
        self._number_nutrition: int = 13
        self._rec_id = self._recipe_id()
        self._soup = BeautifulSoup(self.html_source, "html.parser")

    def _recipe_id(self) -> Union[int, float]:
        '''Locates the recipe id'''
        try:
            return int(findall(r"\d+", self.url)[0])
        except Exception:
            return nan

    def recipe_id(self) -> Union[int, float]:
        '''Returns the recipe id, public method'''
        try:
            return self._rec_id
        except Exception:
            return nan

    def recipe_name(self) -> Union[str, float]:
        '''Locates the recipe name'''
        try:
            recipe_name = str(self._soup.find("h1").text.strip())

            return recipe_name
        except Exception:
            return nan

    def ingredients(self) -> Union[list[str], float]:
        '''Locates the ingredients and its count'''
        try:
            ingredients = []
            struct_ingred = self._soup.select(
                ".mntl-structured-ingredients__list li"
                )
            for li in struct_ingred:
                ingred = li.get_text(strip=True, separator=" ")
                ingredients.append(ingred)
            self.ingredients_number = int(len(ingredients))
            return ingredients
        except Exception:
            return nan

    def n_ingredients(self) -> Union[int, float]:
        '''Returns ingredients count'''
        try:
            return self.ingredients_number
        except Exception:
            return nan

    def steps(self) -> Union[list[str], float]:
        '''Locates the steps with cooking process and its count'''
        try:
            steps = []
            tmp = self._soup.find_all(
                "p", "comp mntl-sc-block mntl-sc-block-html"
                )
            for el in tmp:
                step = el.get_text(strip=True, separator=" ")
                steps.append(step)
            self.steps_number = int(len(steps))
            return steps
        except Exception:
            return nan

    def n_steps(self) -> Union[int, float]:
        '''Locates steps count'''
        try:
            return self.steps_number
        except Exception:
            return nan

    def minutes(self) -> Union[int, float]:
        '''Return total time cooking'''
        try:
            total_time = self._soup.find(
                class_='mntl-recipe-details__label', text='Total Time:'
            ).find_next_sibling().text.strip()
            time_list = total_time.split(" ")
            try:
                ind_day = time_list.index('day')
                hrs_in_day = int(time_list[ind_day - 1]) * 24 * 60
            except ValueError:
                hrs_in_day = 0
            try:
                ind_hrs = time_list.index('hrs')
                hrs_in_min = int(time_list[ind_hrs - 1]) * 60
            except ValueError:
                hrs_in_min = 0
            try:
                ind_mins = time_list.index('mins')
                mins = int(time_list[ind_mins - 1])
            except ValueError:
                mins = 0
            return hrs_in_day + hrs_in_min + mins

        except Exception:
            return nan

    def tags(self) -> Union[str, float]:
        '''Locates tags about recipe'''
        try:
            tags = self._soup.find(
                class_="breadcrumbs__scroll-wrapper").get_text(
                strip=True, separator="/"
            )
            return str(tags.split("/")[:-1])
        except Exception:
            return nan

    def calories(self) -> Union[int, float]:
        '''Locates calories'''
        try:
            calories = self._soup.find(
                class_="mntl-nutrition-facts-label__calories"
            ).get_text(strip=True, separator=" ")
            return int(calories.split(" ")[1])
        except Exception:
            return nan

    def nutrition(self) -> Union[list[str], float]:
        '''Locates all nutritions info'''
        try:
            nutritions = []
            for i in range(1, self._number_nutrition + 1):
                tag = self._soup.select(
                 f".mntl-nutrition-facts-label__table-body > tr:nth-child({i})"
                 )
                for el in tag:
                    nutrit = el.get_text(strip=True, separator=" / ")

                    nutritions.append(nutrit)
            return nutritions
        except Exception:
            return nan

    def interaction_recipe(self) -> Union[Interactions_dict, float]:
        '''Return dict with review info for current recipe page'''
        interactions: dict[str, list[Union[int, str, None]]] = {
            "user_id": [],
            "date": [],
            "rating": [],
            "review": [],
            "recipe_id": [],
        }
        try:
            all_feedback_items = self._soup.find(class_="feedback-list__items")
            count_reviews = self._soup.find(
                id="mntl-recipe-review-bar__comment-count_1-0"
            ).get_text(strip=True)
            count_reviews = int(count_reviews.split()[0])
            if count_reviews > self.max_review:
                count_reviews = self.max_review - self._num_rev_before_scroll

            for n_child in range(1, count_reviews + 1):
                feedbacklist_item = all_feedback_items.select_one(
                    f"div.feedback-list__item:nth-child({n_child})"
                )
                try:
                    user_id = (feedbacklist_item
                               .find(
                                class_='feedback__display-name-link'
                                )
                               .get("href")
                               .split('/')[-1])
                    interactions["user_id"].append(int(user_id))
                except Exception:
                    continue
                interactions["review"].append(
                    feedbacklist_item.find(
                        class_="feedback__text"
                    ).text.strip()
                )
                interactions["date"].append(
                    feedbacklist_item.find(
                        class_="feedback__meta-date"
                    ).text.strip()
                )
                interactions["rating"].append(
                    len(feedbacklist_item.find_all(class_="ugc-icon-star"))
                )
                interactions["recipe_id"].append(int(self._rec_id))
            return interactions
        except Exception:
            return nan
