import requests
from bs4 import BeautifulSoup
import pandas as pd
from typing import Optional


def get_url(urls: list[str],
            path_to_url: Optional[str] = None) -> pd.DataFrame:
    '''Collects recipe url from the recipe section of
        www.allrecipes.com into pd.DataFrame.
    '''

    for url in urls:
        page = requests.get(url)
        soup = BeautifulSoup(page.text, "html.parser")
        recipe_urls = pd.Series([a.get("href") for a in soup.find_all("a")])
        recipe_urls = recipe_urls[
            (recipe_urls.str.count("-") > 0) & (
                recipe_urls.str.contains("/recipe/")
            )
        ].unique()
        r = locals()
        t = globals()
        if 'df' not in locals():
            df = pd.DataFrame({"recipe_urls": recipe_urls})
            # df["recipe_urls"] = df["recipe_urls"].astype("str")
        else:
            df = pd.concat([df,
                            pd.DataFrame({"recipe_urls": recipe_urls})
                            ],
                           ignore_index=True)
    df["recipe_urls"] = df["recipe_urls"].astype("str")
    if path_to_url:
        df.to_csv(path_to_url, sep="\t", index=False)

    return df


if __name__ == "__main__":

    pass
    # df_url_to_parse = get_url(config.START_URL, config.PATH_TO_URL)

    '''if exists(config.PATH_TO_URL):
        df_url_to_parse = pd.read_csv(config.PATH_TO_URL)
    else:
        df_url_to_parse = get_url(config.START_URL, config.PATH_TO_URL)'''
