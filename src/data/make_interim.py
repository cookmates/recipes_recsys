# -*- coding: utf-8 -*-
import click
import logging
import pandas as pd
import os
from pathlib import Path
from src import config


@click.command()
@click.argument('input_filepath_kaggle_recipe',
                default=config.PATH_KAGGLE_RECIPE,
                type=click.Path(exists=True))
@click.argument('input_filepath_kaggle_interactions',
                default=config.PATH_KAGGLE_INTERACTIONS,
                type=click.Path(exists=False))
@click.argument('input_filepath_scrape_recipe',
                default=config.PATH_SCRAPED_RECIPE,
                type=click.Path(exists=False))
@click.argument('input_filepath_scrape_interactions',
                default=config.PATH_SCRAPED_INTERACTIONS,
                type=click.Path(exists=False))
@click.argument('output_filepath',
                default=config.PATH_INTERIM,
                type=click.Path())
def main(input_filepath_kaggle_recipe: str,
         input_filepath_scrape_recipe: str,
         input_filepath_kaggle_interactions: str,
         input_filepath_scrape_interactions: str,
         output_filepath: str) -> None:
    """ Runs data processing scripts to turn data from
        (../raw and ../external) into merge data which ready
        to be cleaned (saved in ../interim).
    """
    logger = logging.getLogger(__name__)
    logger.info('making interim dataset from raw/external data')

    # Reads the dataframe and creates a compound key like
    # 'table_id'_'recipe_id' for recipe_kaggle.
    recipe_kaggle = pd.read_csv(input_filepath_kaggle_recipe)
    recipe_kaggle = set_key(recipe_kaggle, 1, 'id')

    # Expand nutrition column in several columns.
    nutrition = [
             'calories',
             'total fat (PDV)',
             'sugar (PDV)',
             'sodium (PDV)',
             'protein (PDV)',
             'saturated fat (PDV)',
             'carbohydrates (PDV)'
            ]
    recipe_kaggle[nutrition] = (((recipe_kaggle.nutrition.str.strip('[]'))
                                .str.split(",", expand=True))
                                .astype(float))

    # Reads the dataframe and creates two compound key as
    # 'table_id'_'recipe_id' and 'table_id'_'user_id' for interactions_kaggle.
    interactions_kaggle = pd.read_csv(input_filepath_kaggle_interactions)
    interactions_kaggle["date"] = pd.to_datetime(interactions_kaggle["date"])
    interactions_kaggle = set_key(interactions_kaggle, 1, 'recipe_id')
    interactions_kaggle = set_key(interactions_kaggle, 1, 'user_id')

    # Reads the dataframe and creates a compound key like
    # 'table_id'_'recipe_id' for recipe_scrape.
    recipe_scrape = pd.read_csv(input_filepath_scrape_recipe)
    recipe_scrape = set_key(recipe_scrape, 2, 'recipe_id')

    # Reads the dataframe and creates two compound key as
    # 'table_id'_'recipe_id' and 'table_id'_'user_id' for interactions_scrape
    # Converts date into datetime.
    interactions_scrape = pd.read_csv(input_filepath_scrape_interactions)
    interactions_scrape["date"] = pd.to_datetime(interactions_scrape["date"])
    interactions_scrape = set_key(interactions_scrape, 2, 'recipe_id')
    interactions_scrape = set_key(interactions_scrape, 2, 'user_id')

    # Renames columns in recipe_scrape like recipe_kaggle.
    recipe_scrape = recipe_scrape.rename(columns={'recipe_name': 'name',
                                                  'recipe_id': 'id'})

    # Concatenates dataframes.
    df_recipes = pd.concat([recipe_kaggle, recipe_scrape],
                           axis=0, join='inner')
    df_interactions = pd.concat([interactions_kaggle, interactions_scrape],
                                axis=0, join='inner')

    # Saves dataframes.
    (pd.DataFrame(data=df_interactions)
        .to_csv(f"{output_filepath}/interactions.csv", index=False))
    (pd.DataFrame(data=df_recipes)
        .to_csv(f"{output_filepath}/recipes.csv", index=False))


def set_key(df: pd.DataFrame,
            key_table: int,
            name_key: str,
            set_index: bool = False) -> pd.DataFrame:
    '''Gets the dataset, key name ('recipe_id' for example) and key table
        to create the compound key.
        Returns data as a source dataframe with the two new colomns
        (id_table,compound_key) and original index key,
        or with a new compound key as an index key (option 'set_index' = True).
        Example compound key:
        If name_key = 'recipe_id', key_table = 1 and recipe_id = 40893:
        compound_key = 1_40893.

            :param df: source dataframe
            :param key_table: identificator table for other part of
             compound key
            :param name_key: colomns name to create part of compound key
            :param set_index: flag to set new compound key as index
        '''
    df['id_table'] = key_table
    compound_key = df.apply(lambda row: (str(row['id_table'])
                                         + '_' + str(row[name_key])
                                         ), axis=1)
    if set_index:
        df.index = pd.Index(compound_key)

    df['compound_key'] = pd.DataFrame(compound_key)
    return df


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    project_dir = Path(__file__).resolve().parents[2]
    os.chdir(project_dir)

    main()
