# -*- coding: utf-8 -*-
from typing import Any

import pandas as pd


TEST_SIZE = 0.25
COLUMNS = ["user_id", "recipe_id", "rating"]


def train_test_split(df_interactions: pd.DataFrame) -> tuple[Any, Any]:
    """Split data into train and test datsets."""

    split_idx = int(df_interactions.shape[0] * (1 - TEST_SIZE))
    df_train = df_interactions.iloc[:split_idx]
    df_test = df_interactions.iloc[split_idx:]

    return df_train[COLUMNS], df_test[COLUMNS]
