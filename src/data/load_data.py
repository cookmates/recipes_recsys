import click
import logging
import os
from dotenv import find_dotenv, load_dotenv
from pathlib import Path
from pandas import read_csv
from src.data.scraping import scrape, get_url
from src import config


@click.command()
@click.argument('name_dataset',
                default=config.DATASET_KAGGLE_NAME,
                type=str)
@click.argument('filepath_scrape_recipe',
                default=config.PATH_SCRAPED_RECIPE,
                type=click.Path(exists=False))
@click.argument('filepath_scrape_interactions',
                default=config.PATH_SCRAPED_INTERACTIONS,
                type=click.Path(exists=False))
def main(name_dataset: str,
         filepath_scrape_recipe: str,
         filepath_scrape_interactions: str) -> None:
    """ Runs data load / scrabe scripts to get raw data from kaggle and
        AllRecipes site and saves.
    """
    logger = logging.getLogger(__name__)
    logger.info('making load data')

    # Gets and saves kaggle dataset.
    get_kaggle_dataset(name_dataset)

    # Сhecks list of addresses to scrape.
    if os.path.exists(config.PATH_TO_URL):
        df_url_to_parse = read_csv(config.PATH_TO_URL)
    else:
        df_url_to_parse = get_url.get_url(
                                          config.START_URL,
                                          config.PATH_TO_URL
                                          )

    # Scrapes data from www.allrecipes.com,
    # collects two dataframes with recipe and interaction data,
    # saves it in external.
    scrape.scrape(df_url_to_parse,
                  filepath_scrape_recipe,
                  filepath_scrape_interactions)


def get_kaggle_dataset(dataset: str) -> None:

    from kaggle.api.kaggle_api_extended import KaggleApi

    api = KaggleApi()
    api.authenticate()
    api.dataset_download_files(dataset, path="data/raw", unzip=True)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    load_dotenv(find_dotenv())
    os.getenv("KAGGLE_USERNAME")
    os.getenv("KAGGLE_KEY")

    project_dir = Path(__file__).resolve().parents[2]
    os.chdir(project_dir)

    main()
