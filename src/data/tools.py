# -*- coding: utf-8 -*-
from typing import List

import pandas as pd

MAIN_DISH_TAGS = ["main-dish", "Main Dishes"]
DESSERTS_TAGS = ["desserts", "Desserts"]
VEGETARIAN_TAGS = ["vegan", "vegetarian", "Vegetarian"]
DIETARY_TAGS = ["dietary"]


def report_nunique(df: pd.DataFrame,
                   stage_name: str) -> None:

    unique_recipes_id = df["recipe_id"].nunique()
    unique_users_id = df["user_id"].nunique()

    print(f"{stage_name} number of unique recipes: {unique_recipes_id}")
    print(f"{stage_name} number of unique users: {unique_users_id}")


def remove_min_ratings(df: pd.DataFrame,
                       col: str,
                       min_ratings_threshold: int) -> List[int]:

    ratings_per_id = df.groupby(col)[col].count()
    mask = ratings_per_id > min_ratings_threshold
    ids = list(ratings_per_id[mask].index)
    print(f"Removed {col} with less than {min_ratings_threshold} ratings")
    return ids


def clean_dataset(df: pd.DataFrame,
                  min_ratings_per_recipe: int,
                  min_ratings_per_user: int) -> pd.DataFrame:
    """Clean the raw dataset.
        - sort by date
        - clean from rating = 0
        - clean from recipes with less than min_ratings_per_recipe
        - clean from users with less than min_ratings_per_user
    """
    report_nunique(df, "Initial")

    # sort by date
    df.sort_values("date", inplace=True)

    # remove 0-ratings
    df = df[df["rating"] != 0]

    # remove recipes with less than min_ratings_per_recipe
    recipe_ids = remove_min_ratings(df, "recipe_id", min_ratings_per_recipe)
    df = df[df["recipe_id"].isin(recipe_ids)]

    # remove users with less than min_ratings_per_user
    user_ids = remove_min_ratings(df, "user_id", min_ratings_per_user)
    df = df[df["user_id"].isin(user_ids)]
    report_nunique(df, "Final")

    return df


def check_tags(df: pd.DataFrame,
               tags_to_check: List[str]) -> pd.Series:
    """Check if any of the tags_to_check are in tags."""
    return df["tags"].map(lambda x: any([_ for _ in x if _ in tags_to_check]))


def add_tags(df: pd.DataFrame) -> pd.DataFrame:
    """Add tags to recipes_df.

    The following columns with bool values are added:
    1.Preparation time tags:
    - "15-minutes-or-less"
    - "30-minutes-or-less"
    - "60-minutes-or-less"
    - "more-than-60-minutes"
    2. Meal type
    - "maindish"
    - "dessert"
    3. Vegetarian
    - "vegetarian"
    4. Dietary
    - "dietary"
    """
    df["15-minutes-or-less"] = df["minutes"] < 15
    df["30-minutes-or-less"] = df["minutes"] < 30
    df["60-minutes-or-less"] = df["minutes"] < 60
    df["more-than-60-minutes"] = df["minutes"] > 60

    df["maindish"] = check_tags(df, MAIN_DISH_TAGS)
    df["dessert"] = check_tags(df, DESSERTS_TAGS)

    df["vegetarian"] = check_tags(df, VEGETARIAN_TAGS)
    df["dietary"] = check_tags(df, DIETARY_TAGS)

    return df
