import os
from src.config import DATASET_KAGGLE_NAME


def get_kaggle_dataset(dataset: str) -> None:

    from kaggle.api.kaggle_api_extended import KaggleApi

    api = KaggleApi()
    api.authenticate()
    api.dataset_download_files(dataset, path="data/raw", unzip=True)


if __name__ == "__main__":
    os.getenv("KAGGLE_USERNAME")
    os.getenv("KAGGLE_KEY")
    get_kaggle_dataset(DATASET_KAGGLE_NAME)
