# -*- coding: utf-8 -*-
import click
import logging
import pandas as pd

from src.config import PATH_INTERIM, PATH_PROCESSED
from src.data.train_test_split import train_test_split
from src.data.tools import clean_dataset, add_tags


@click.command()
@click.argument("input_filepath_interactions",
                default=f"{PATH_INTERIM}/interactions.csv",
                type=click.Path(exists=True))
@click.argument("input_filepath_recipes",
                default=f"{PATH_INTERIM}/recipes.csv",
                type=click.Path(exists=True))
@click.argument("output_filepath",
                default=PATH_PROCESSED,
                type=str)
@click.argument("min_ratings_per_recipe", default=1, type=int)
@click.argument("min_ratings_per_user", default=1, type=int)
def main(input_filepath_interactions: str,
         input_filepath_recipes: str,
         output_filepath: str,
         min_ratings_per_recipe: int,
         min_ratings_per_user: int) -> None:
    """ Runs data processing scripts to turn interim datasets from (../interim)
        into final dataset ready for model training (saved in ../processed).
    """
    logger = logging.getLogger(__name__)
    logger.info("making final data set from interim data")

    # Read datasets
    df_interactions = pd.read_csv(input_filepath_interactions)
    df_recipes = pd.read_csv(input_filepath_recipes,
                             converters={"tags": pd.eval})

    # Clean and process datasets
    df_interactions = clean_dataset(df_interactions,
                                    min_ratings_per_recipe,
                                    min_ratings_per_user)
    df_recipes = add_tags(df_recipes)

    # Split interactions into train and test
    df_train, df_test = train_test_split(df_interactions)

    # Save datasets
    (pd.DataFrame(data=df_train)
        .to_csv(f"{output_filepath}/interactions_train.csv", index=False))
    (pd.DataFrame(data=df_test)
        .to_csv(f"{output_filepath}/interactions_test.csv", index=False))
    (pd.DataFrame(data=df_recipes)
        .to_csv(f"{output_filepath}/recipes.csv", index=False))


if __name__ == "__main__":

    log_fmt = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    main()
