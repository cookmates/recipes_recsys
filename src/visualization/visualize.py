import pandas as pd
import matplotlib.pyplot as plt


def plot_share_bar(y_train: pd.DataFrame, y_test: pd.DataFrame) -> None:
    """Bar plot comparing shares of each rating value in y_train and y_test."""
    ax = plt.subplot(111)

    ax.bar(x=y_train["rating"].value_counts(normalize=True).index - 0.1,
           height=y_train["rating"].value_counts(normalize=True).values,
           alpha=0.3, label="y_train", width=0.5, align='center')

    ax.bar(x=y_test["rating"].value_counts(normalize=True).index + 0.1,
           height=y_test["rating"].value_counts(normalize=True).values,
           alpha=0.3, label="y_test", width=0.5, align='center')

    plt.legend()
