DATASET_KAGGLE_NAME = "shuyangli94/food-com-recipes-and-user-interactions"

START_URL = [
  "https://www.allrecipes.com/recipes/728/world-cuisine/latin-american/mexican/",  # noqa
  "https://www.allrecipes.com/recipes/716/world-cuisine/european/eastern-european/russian/",  # noqa
  "https://www.allrecipes.com/recipes/723/world-cuisine/european/italian/"
  ]


PATH_TO_URL = "src/data/scraping/recipe_urls.csv"

NAME_SCRAPED_SITE = "AllRecipes"

PATH_SCRAPED_RECIPE = f"data/external/raw_recipes_{NAME_SCRAPED_SITE}.csv"

PATH_SCRAPED_INTERACTIONS = "data/external/raw_interaction_{}.csv".format(
                                NAME_SCRAPED_SITE
                                )

PATH_KAGGLE_RECIPE = "data/raw/RAW_recipes.csv"

PATH_KAGGLE_INTERACTIONS = "data/raw/RAW_interactions.csv"

PATH_INTERIM = "data/interim"

PATH_PROCESSED = "data/processed"

PATH_MODELS = "models"

PATH_REPORTS = "reports"

TFIDF_ENCODING_PATH = "models/tfidf_encodings.pkl"

TFIDF_MODEL_PATH = "models/tfidf.pkl"

PATH_INGRED_PARSE = "data/interim/parse_ingred_{}.csv".format(
                                NAME_SCRAPED_SITE
                                )

GET_DEBUG_INFO = True

LOGGING = True

MODE = "NORMAL"

CONNECTION_TYPE = "POLLING"

PROJECT_VER = "0.1.0"
